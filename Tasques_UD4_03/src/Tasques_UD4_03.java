/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Tasques_UD4_03 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int x = 30, y = 45;
		double n = 60.0, m = 30.0;
		
		System.out.println("x: " + x);
		System.out.println("y: " + y);
		System.out.println("n: " + n);
		System.out.println("m: " + m);
		
		System.out.println();
		System.out.println("La suma de "+ x +" + " + y + " = " + (x + y));
		System.out.println("La diferencia de "+ x +" - " + y + " = " + (x - y));
		System.out.println("El producto de "+ x +" x " + y + " = " + (x * y));
		System.out.println("El cociente de "+ x +" / " + y + " = " + (x / y));
		System.out.println("El resto de "+ x +" % " + y + " = " + (x % y));
		
		System.out.println();
		System.out.println("La suma de "+ n +" + " + m + " = " + (n + m));
		System.out.println("La diferencia de "+ n +" - " + m + " = " + (n - m));
		System.out.println("El producto de "+ n +" x " + m + " = " + (n * m));
		System.out.println("El cociente de "+ n +" / " + m + " = " + (n / m));
		System.out.println("El resto de "+ n +" % " + m + " = " + (n % m));
		
		System.out.println();
		System.out.println("La suma de "+ x +" + " + n + " = " + (x + n));
		System.out.println("El cociente de "+ y +" / " + m + " = " + (y / m));
		System.out.println("El resto de "+ y +" % " + m + " = " + (y % m));
		
		System.out.println();
		System.out.println("El doble de: " + x + " es " + (x * 2));
		System.out.println("El doble de: " + y + " es " + (y * 2));
		System.out.println("El doble de: " + n + " es " + (n * 2));
		System.out.println("El doble de x: " + m + " es " + (m * 2));
		
		System.out.println();
		System.out.println("La suma de todas las variables es: " + (x + y + n + m));
		System.out.println("El producto de todas las variables es: " + (x * y * n * m));

	
	}

}
