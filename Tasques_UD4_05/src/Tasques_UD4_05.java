/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Tasques_UD4_05 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int a = 20;
		int b = 30;
		int c = 40;
		int d = 50;
		
		//Reasignamos cambiando los valores de cada variable intercambiando sus valores
		
		b = c;
		c = a;
		a = d;
		d = b;
		
		System.out.println("El valor final de a es: " + a);
		System.out.println("El valor final de b es: " + b);
		System.out.println("El valor final de c es: " + c);
		System.out.println("El valor final de d es: " + d);

	}

}
