
public class Tasques_UD4_01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int num1 = 10;
		int num2 = 5;
		
		System.out.println("La suma de " + num1 + " + " + num2 + " = " + (num1 + num2));
		
		System.out.println("La resta de " + num1 + " - " + num2 + " = " + (num1 - num2));
		
		System.out.println("La multiplicación de " + num1 + " x " + num2 + " = " + (num1 * num2));
		
		System.out.println("El módulo de " + num1 + " por " + num2 + " = " + (num1 % num2));

	}

}
