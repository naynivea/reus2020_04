/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Tasques_UD4_04 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n = 13;
		
		System.out.println("El valor inicial de n = " + n);
		
		//Incrementamos el valor de n + 77
		n += 77;
		System.out.println("El valor de n despu�s de incrementar 77 es: " + n);
		
		//Decrementamos 3 de n
		n -= 3;
		System.out.println("El valor de n despu�s de decrementar 3 es de: " + n);
		
		//Duplicamos el valor de n
		n *= 2;
		System.out.println("El valor de n despu�s de duplicarlo es de: " + n);
		
		
	
		
	}

}
